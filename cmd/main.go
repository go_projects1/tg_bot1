package main

import (
	"fmt"
	"log"
	"time"

	tb "gopkg.in/tucnak/telebot.v2"
)

func main() {
	b, err := tb.NewBot(tb.Settings{
		// You can also set custom API URL.
		// If field is empty it equals to "https://api.telegram.org".
		//URL: "http://195.129.111.17:8012",

		Token:  "2123013715:AAH9UOcE2Za2FWDwv1HBScn9Sf5ajcJyRx0",
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	// Command: /start <PAYLOAD>
	b.Handle("/start", func(m *tb.Message) {
		if !m.Private() {
			return
		}

		fmt.Println(m.Payload) // <PAYLOAD>
	})

	b.Handle("/hello", func(m *tb.Message) {
		b.Send(m.Sender, "Hello is from golang programm!")
	})

	b.Handle("/opt", func(m *tb.Message) {
		// regular send options
		b.Send(m.Sender, "Choose", &tb.SendOptions{
			// ...
		})
	})

	b.Handle(tb.OnText, func(m *tb.Message) {
		// all the text messages that weren't
		// captured by existing handlers
		fmt.Println(m.Payload)
		fmt.Println("On text")
		b.Send(m.Sender,m.Text)
	})

	b.Handle(tb.OnPhoto, func(m *tb.Message) {
		// photos only
		fmt.Println(m.Payload)
		fmt.Println("On photo")
		b.Send(m.Sender,m.Text)
	})

	b.Handle(tb.OnChannelPost, func (m *tb.Message) {
		// channel posts only
		fmt.Println(m.Payload)
		fmt.Println("On channel")
		b.Send(m.Sender,m.Text)
	})

	b.Handle(tb.OnQuery, func (q *tb.Query) {
		// incoming inline queries
		fmt.Println(q.Text)
		fmt.Println("On query")

	})
	///////////////////////////


	////////////////////////
	b.Start()
}
